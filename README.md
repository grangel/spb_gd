# SPB

## Basado en Bootstrap 4, Thymeleaf 3 y Spring Boot 2, optimizado para JBoss.

### Solicitar los siguientes permisos en caso de ser necesarios:

Servicio web para obtener los usuarios ldap para la adminsitración de usuario: 

```

Servidor ldap:

```
```

Servidor de recursos web (no los soliciten a menos de que sea necesario, por defecto debería de trabajar sin problema el sistema)
```


### Para compilar ejecuta lo siguiente desde la terminal en el directorio raiz del proyecto

Desarrollo:
```
mvn clean package -Pdev
```

QA y Producción :
```
mvn clean package -Pprod
```

### Para desplegar de manera local utiliza el servidor Tomcat embebido en el IDE de tu preferencia.

## Conexion a base de datos


### Importante

**Recordar trabajar y subir sus cambios a las ramas correctas**

Ambiente de desarrollo -> Branch development
```
git checkout development
```
Ambiente de calidad  -> Branch qa
```
git checkout qa
```
Ambiente **PRODUCTIVO**
```
git checkout master
```

